# change this to the file name of the paper
PRJ = performance

all : pdf
pdf : ${PRJ}.pdf

${PRJ}.pdf: *.tex ref.bib figures/*
	rm -rf ${PRJ}.aux
	pdflatex -halt-on-error ${PRJ} && bibtex ${PRJ} && pdflatex -halt-on-error ${PRJ} && pdflatex -halt-on-error ${PRJ}

view : pdf
	open ${PRJ}.pdf &

# change this to include what should be in the tarball,  use --exclude for exclusion.
pack : clean
	cd ..; /bin/rm -f ${PRJ}.tgz; tar -hzcf ${PRJ}.tgz ${PRJ}

clean:
	/bin/rm -f *.toc *.aux *.bbl *.blg *.log *.dvi *~* *.bak

distclean:
	/bin/rm -f *.toc *.aux *.bbl *.blg *.log ${PRJ}.dvi ${PRJ}.ps ${PRJ}.pdf *~* *.bak

